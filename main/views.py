from django.shortcuts import render,redirect
from .forms import KuliahForm
from .models import Kuliah


def home(request):
    if request.method == "POST":
        form = KuliahForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('main:home')
    else:
        form = KuliahForm
    context = {'form' : form}
    return render(request, 'main/home.html', context)

def display_matkul(request):

    isi = Kuliah.objects.all()
    return render(request, 'main/displayMatkul.html', {'isi' : isi})

def del_matkul(request, mata_kuliah):
    isi = Kuliah.objects.filter(mata_kuliah = mata_kuliah)
    isi.delete()
    return redirect('main:home')

def detail_matkul(request, mata_kuliah):
    isi = Kuliah.objects.filter(mata_kuliah = mata_kuliah)
    return render(request, 'main/detailMatkul.html', {'isi' : isi})